//
//  Game.swift
//  SetGame2
//
//  Created by Ionut Flocea on 01.03.2022.
//

import Foundation
import UIKit
 
struct Game{
    
    private var deck = Deck()
    private(set) var cardsOnTable: [Card] = []
    private(set) var cardsPressed: [Card] = []
  //  private(set) var numerOfCardsOnTable = 12
    
    init() {
        for _ in 0...11 {
            cardsOnTable.append(deck.drawCard())
        }
    }
    
   mutating func add3Cards() {
        for _ in 0...2 {
            cardsOnTable.append(deck.drawCard())
        }
    }
    
    
    func checkIfSet() -> Bool{
        return (cardsPressed[0].shapeNumber.rawValue + cardsPressed[1].shapeNumber.rawValue + cardsPressed[2].shapeNumber.rawValue) % 3 == 0
                && (cardsPressed[0].shape.rawValue  + cardsPressed[1].shape.rawValue  + cardsPressed[2].shape.rawValue ) % 3 == 0
                && (cardsPressed[0].shade.rawValue  + cardsPressed[1].shade.rawValue  + cardsPressed[2].shade.rawValue ) % 3 == 0
                && (cardsPressed[0].color.rawValue  + cardsPressed[1].color.rawValue  + cardsPressed[2].color.rawValue ) % 3 == 0
    }
    
    mutating func removeAllPressedCards(){
        cardsPressed.removeAll()
    }
    
    mutating func removeFromPressedCards(id: Int){
        let index = cardsOnTable.firstIndex(where: {$0.ID == id})!
        cardsPressed.removeAll(where: {$0.ID == cardsOnTable[index].ID})
    }
    
    mutating func addInPressedCards(id: Int){
        let index = cardsOnTable.firstIndex(where: {$0.ID == id})!
        cardsPressed.append(cardsOnTable[index])
    }
    
    mutating func replaceCardsIfSet(){
        for i in 0...2{
            replaceCard(card: cardsPressed[i])
        }
    }
    
    mutating func removeCardsIfSet(){
        for i in 0...2{
            cardsOnTable.removeAll(where: {$0.ID == cardsPressed[i].ID})
        }
    }
    
    mutating func replaceCard(card: Card){
        let index = cardsOnTable.firstIndex(where: {$0.ID == card.ID})!
        cardsOnTable[index] = deck.drawCard()
    }
    
    
    
    
}

