//
//  Card.swift
//  SetGame2
//
//  Created by Ionut Flocea on 01.03.2022.
//

import Foundation

class Card: Equatable {
    
    private(set) var shape: Shape
    private(set) var shade: Shade
    private(set) var shapeNumber: ShapeNumber
    private(set) var color: Color
    private(set) var isPressed: Bool
    static var IDfactory = 0
    var ID: Int

    
    init(shape: Shape, shade: Shade, shapeNumber: ShapeNumber, color: Color) {
        self.shape=shape
        self.shade=shade
        self.shapeNumber=shapeNumber
        self.color=color
        self.isPressed = false
        self.ID = Card.getUniqueID()
    }
    
    static func getUniqueID() -> Int {
        Card.IDfactory+=1
        return Card.IDfactory
    }
    
    func changeIsPressedStatus() {
        switch isPressed{
        case true: isPressed = false
        case false: isPressed = true
        }
    }
    
    func setIsPressedStatus(status: Bool) {
        self.isPressed = status
    }
    
    
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return (lhs.shape.rawValue == rhs.shape.rawValue) &&
        (lhs.color.rawValue == rhs.color.rawValue) &&
        (lhs.shade.rawValue == rhs.shade.rawValue) &&
        (lhs.shapeNumber.rawValue == rhs.shapeNumber.rawValue)
    }

}
