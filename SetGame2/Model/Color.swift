//
//  Color.swift
//  SetGame2
//
//  Created by Ionut Flocea on 01.03.2022.
//

import Foundation
import UIKit

enum Color: Int, CaseIterable {
    case red = 0
    case blue = 1
    case green = 2
    
    func uiColor() -> UIColor {
        switch self {
        case .red:
            return UIColor.red
        case .blue:
            return UIColor.blue
        case .green:
            return UIColor.green
        }
    }
}
