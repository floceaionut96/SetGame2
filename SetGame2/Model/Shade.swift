//
//  Shade.swift
//  SetGame2
//
//  Created by Ionut Flocea on 01.03.2022.
//

import Foundation
enum Shade: Int, CaseIterable {
    case solid = 0
    case striped = 1
    case open = 2
    
}
