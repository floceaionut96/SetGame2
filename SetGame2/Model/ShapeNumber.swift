//
//  ShapeNumber.swift
//  SetGame2
//
//  Created by Ionut Flocea on 01.03.2022.
//

import Foundation
enum ShapeNumber: Int, CaseIterable {
    case one = 0
    case two = 1
    case three = 2
    
    func noOfShapes() -> Int {
        switch self {
        case .one:
            return 1
        case .two:
            return 2
        case .three:
            return 3
        }
    }
}
