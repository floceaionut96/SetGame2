//
//  Deck.swift
//  SetGame2
//
//  Created by Ionut Flocea on 01.03.2022.
//

import Foundation
import UIKit
struct Deck {
    
    private var deck: [Card] = []

    init() {
        for number in ShapeNumber.allCases {
            for shape in Shape.allCases {
                for color in Color.allCases {
                    for shade in Shade.allCases {
                        let card = Card(shape: shape, shade: shade, shapeNumber: number, color: color)
                        deck.append(card)
                    }
                }
            }
        }
        deck.shuffle()
    }
    
   mutating func removeFromDeck (card: Card) {
       self.deck = deck.filter { $0 != card }
    }
    
    mutating func drawCard() -> Card {
        let card = deck[0]
        removeFromDeck(card: card)
        return card
    }
    
}
