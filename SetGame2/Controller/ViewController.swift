//
//  ViewController.swift
//  SetGame2
//
//  Created by Ionut Flocea on 01.03.2022.
//

import UIKit

class ViewController: UIViewController {

    private var game = Game()
    var cardsView:[[CardView]] = []
    var setNumbers = 0
    @IBOutlet weak var cardsFrame: UIView!
    @IBOutlet weak var setsButton: UILabel!
    var pressedViews: [CardView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateCardsViews()
        relayout()
        
    }
    
    @IBAction func drawButton(_ sender: UIButton) {
        if game.cardsOnTable.count < 24 {
        game.add3Cards()
        addRow()
        relayout()
        } else {
            
        }
    }
    private func relayout(){
        var count = 0
        let grid = Grid(layout: .dimensions(rowCount: cardsView.count, columnCount: 3), frame: cardsFrame.bounds)
        for i in 0...cardsView.count-1{
            for j in 0...2{
                let cardView = cardsView[i][j]
                cardView.frame = grid[i, j]!
                cardView.createCard(card: game.cardsOnTable[count])
                cardView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(pressCard(_:))))
                highlightview(view: cardView, highlight: false)
                cardView.isPressed = false
                cardView.removeFromSuperview()
                cardsFrame.addSubview(cardView)
                count += 1
            }
        
    }
    }
        
        func populateCardsViews(){
            for _ in 1...4{
                cardsView.append([CardView(),
                                   CardView(),
                                  CardView()])
            }
        }
        func addRow(){
            cardsView.append([CardView(),
                               CardView(),
                              CardView()])
            
        }
    func removeRow(){
        cardsView.remove(at: cardsView.count-1)
    }
                                                  
    @objc func pressCard(_ recognizer: UITapGestureRecognizer){
        switch recognizer.state {
        case .ended:
            if let chosenCardView = recognizer.view as? CardView {
                
//                printCardsInTable()
//                printCardsPressed()
                
                if game.cardsPressed.count == 3{
                    if game.checkIfSet(){
                        if game.cardsOnTable.count <= 12{
                    game.replaceCardsIfSet()
                        } else {
                            removeViews()
                            removeRow()
                            print("No Cards before set \(game.cardsOnTable.count)")
                            game.removeCardsIfSet()
                            print("No Cards after set \(game.cardsOnTable.count)")
                        }
                    game.removeAllPressedCards()
                    setNumbers += 1
                    updateButtons()
                    relayout()
                    } else {
                        game.removeAllPressedCards()
                        relayout()
                    }
                }
                
                if chosenCardView.isPressed == false{
                    highlightview(view: chosenCardView, highlight: true)
                    chosenCardView.isPressed = true
                    game.addInPressedCards(id: chosenCardView.cardID)
                    pressedViews.append(chosenCardView)
                
                } else {
                    highlightview(view: chosenCardView, highlight: false)
                    chosenCardView.isPressed = false
                    game.removeFromPressedCards(id: chosenCardView.cardID)
                    pressedViews.removeAll(where: {$0  == chosenCardView})
                }
                printCardsPressed()
                
                if game.cardsPressed.count == 3{
                    for i in 0...cardsView.count-1{
                        for j in 0...2{
                            let cardView = cardsView[i][j]
                            if cardView.isPressed == true{
                                for gesture in cardView.gestureRecognizers! {
                                    gesture.isEnabled = false
                                }
                                if game.checkIfSet(){
                                    cardView.layer.borderColor = UIColor.green.cgColor
                                }
                                else{
                                    cardView.layer.borderColor = UIColor.red.cgColor
                                }
                            }
                      }
                   }
                }
                
            }
        default:
            break
        }
            }
    
    func updateButtons(){
        setsButton.text = "Sets: \(setNumbers)"
    }
    
    func highlightview(view: CardView, highlight: Bool){
        if highlight{
        view.layer.borderWidth = 4
        view.layer.borderColor = UIColor.cyan.cgColor
        } else {
            view.layer.borderWidth = 0
        }
    }
    
    func removeViews(){
        for view in pressedViews{
            view.removeFromSuperview()
        }
    }
    
    func printCardsPressed(){
        print("There are \(game.cardsPressed.count) cards pressed:")
        for card in game.cardsPressed{
            print(card.ID)
        }
    }
    
    func printCardsInTable() {
        print("Card On table:")
        for card in game.cardsOnTable{
            print(card.ID)
        }
    }
    
    
}
    

  

