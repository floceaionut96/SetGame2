//
//  CardView.swift
//  SetGame2
//
//  Created by Ionut Flocea on 01.03.2022.
//

import UIKit

class CardView: UIView {
    
    private(set) var shape: Shape = Shape.square
    private(set) var shade: Shade = Shade.striped
    private(set) var shapeNumber: ShapeNumber = ShapeNumber.three
    private(set) var color: Color = Color.red
    var isPressed: Bool = false
    var cardID = 0
    let label = UILabel()
    
    
    override func draw(_ rect: CGRect) {
        
    }
    
    func createCard(card: Card){
        self.shape = card.shape
        self.shade = card.shade
        self.shapeNumber = card.shapeNumber
        self.color = card.color
        self.cardID = card.ID
        self.backgroundColor = .clear
        label.font = label.font.withSize(40)
        label.attributedText = createCardFace()
        label.frame.size = CGSize.zero
        label.sizeToFit()
        label.textAlignment = NSTextAlignment.center;
        addSubview(label)
        label.center = CGPoint(x:self.bounds.size.width  / 2,
                                     y: self.bounds.size.height / 2)
    }
    
    func createCardFace() -> NSAttributedString {
        return NSAttributedString(
            string: createCardText(),
            attributes: createCardAttributes()
        );
    }
    
    func createCardText() -> String {
        switch self.shapeNumber{
        case .one:
            return self.shape.shape()
        case .two:
            return (self.shape.shape() + self.shape.shape())
        case .three:
            return (self.shape.shape() + self.shape.shape() + self.shape.shape())
        }
    }
    
    func createCardAttributes() -> [NSAttributedString.Key: Any]{
        switch self.shade{
        case .solid:
            return [
                NSAttributedString.Key.foregroundColor: self.color.uiColor()
            ]
        case .striped:
            return [
                NSAttributedString.Key.strokeColor: self.color.uiColor(),
                NSAttributedString.Key.foregroundColor: self.color.uiColor().withAlphaComponent(0.2),
                NSAttributedString.Key.strokeWidth: -9.0
            ]
        case .open:
            return [
                NSAttributedString.Key.strokeColor: self.color.uiColor(),
                NSAttributedString.Key.foregroundColor: UIColor.white,
                NSAttributedString.Key.strokeWidth: -9.0
            ]
        }
    }

}
